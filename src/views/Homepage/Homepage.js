import React from "react"
import { Layout, Menu } from 'antd';
import {
    MenuUnfoldOutlined,
    MenuFoldOutlined,
    ContactsOutlined,
    HomeOutlined,
    InfoCircleOutlined
    
} from '@ant-design/icons';

import '../../css/layout.css';

const TableComponent = React.lazy(() => import('../Tables/Table'))

const { Header, Sider, Content } = Layout;

class MainLayout extends React.Component {
    state = {
        collapsed: false,
    };

    toggle = () => {
        this.setState({
            collapsed: !this.state.collapsed,
        });
    };


    render() {
        return (
            <Layout>
                <Sider trigger={null} collapsible collapsed={this.state.collapsed}>
                    <div className="logo" />
                    <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
                        <Menu.Item key="1" icon={<HomeOutlined />}>
                            HomePage
                </Menu.Item>
                        <Menu.Item key="2" icon={<InfoCircleOutlined />}>
                            Information 
                </Menu.Item>
                        <Menu.Item key="3" icon={<ContactsOutlined />}>
                            Users
                </Menu.Item>
               
                    </Menu>
                </Sider>
                <Layout className="site-layout">
                    <Header style={{ padding: 0,backgroundColor:"white" }}>
                        {React.createElement(this.state.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
                            className: 'trigger',
                            onClick: this.toggle,
                        })}
                    </Header>
                    <Content
                        className="site-layout-background"
                        style={{
                            margin: '24px 16px',
                            padding: 24,
                            minHeight: 620,
                        }} >
                     <TableComponent/>
                     
              </Content>
                </Layout>
            </Layout>
        );
    }
}

export default MainLayout;