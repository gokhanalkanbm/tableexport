import React from 'react';
import './App.css';
import 'antd/dist/antd.css';
import { Skeleton } from 'antd';


const loading = () => <div style={{ display: 'flex', justifyContent: 'center', padding: 20 }}><div style={{ width: 560 }}><Skeleton avatar paragraph={{ rows: 4 }} /></div></div>;
const HomePage = React.lazy(() => import('./views/Homepage/Homepage'));


function App() {
  return (
      <React.Suspense fallback={loading()}>
        <HomePage />
      </React.Suspense>
  );
}

export default App;